#include "stdafx.h"
#include "TCPNet.h"
#include "0114QQClient.h"

TCPNet::TCPNet(void)
{
	m_sockClient = NULL;
	m_hThread = NULL;
	m_bFlagQuit = true;
}

TCPNet::~TCPNet(void)
{}

bool TCPNet::InitNetWork()
{
	//1.加载库 
	WORD wVersionRequested;
    WSADATA wsaData;
    int err;

/* Use the MAKEWORD(lowbyte, highbyte) macro declared in Windef.h */
    wVersionRequested = MAKEWORD(2, 2);

    err = WSAStartup(wVersionRequested, &wsaData);
    if (err != 0) {
        /* Tell the user that we could not find a usable */
        /* Winsock DLL.                                  */   
        return false;
    }

/* Confirm that the WinSock DLL supports 2.2.*/
/* Note that if the DLL supports versions greater    */
/* than 2.2 in addition to 2.2, it will still return */
/* 2.2 in wVersion since that is the version we      */
/* requested.                                        */

    if (LOBYTE(wsaData.wVersion) != 2 || HIBYTE(wsaData.wVersion) != 2) {
        /* Tell the user that we could not find a usable */
        /* WinSock DLL.                                  */
     
       UnInitNetWork();
        return false;
    }
  
	//2.创建socket 
	m_sockClient =  socket(AF_INET ,SOCK_STREAM ,IPPROTO_TCP);
	if(INVALID_SOCKET == m_sockClient)
	{
		 UnInitNetWork();
        return false;
	}
	sockaddr_in  addrServer;
	addrServer.sin_addr.S_un.S_addr = inet_addr(SERVER_IP);
	addrServer.sin_family= AF_INET;
	addrServer.sin_port = htons(8899);
	if( SOCKET_ERROR == connect(m_sockClient,(const sockaddr*)&addrServer,sizeof(addrServer)))
	{
		 UnInitNetWork();
          return false;
	}
	//线程
	m_hThread=(HANDLE)_beginthreadex(0,0,&ThreadProc,this,0,0);
	return true;
}

unsigned  __stdcall TCPNet::ThreadProc( void * lpvoid)
{
	TCPNet *pthis = (TCPNet*)lpvoid;
	while(pthis->m_bFlagQuit)
	{
		pthis->RecvData();
	}
	return 0;
}

void TCPNet::UnInitNetWork()
{
	m_bFlagQuit = false;
	if(m_hThread)
	{
		if(WAIT_TIMEOUT == WaitForSingleObject(m_hThread,100))
		    TerminateThread(m_hThread,-1);
		CloseHandle(m_hThread);
		m_hThread = NULL;
	}
	if(m_sockClient)
	{
		closesocket(m_sockClient);
		m_sockClient = NULL;
	}
	  WSACleanup();
}

bool TCPNet::SendData(char *szbuf,int nLen)
{
	if( !szbuf ||nLen <=0 )
		return false;
	if(send(m_sockClient,(const char*)&nLen,sizeof(int),0) <=0)
		return false;
	if(send(m_sockClient,szbuf,nLen,0) <=0)
		return false;
	return true;
}

void TCPNet::RecvData()
{
	int nPackSize;
	int nRelReadNum;
	char* pszbuf= NULL;
	nRelReadNum = recv(m_sockClient,(char*)&nPackSize,sizeof(int),0);
	if(nRelReadNum <=0)
	{
		return;
	}
	pszbuf = new char[nPackSize];
	int offset = 0;
	while(nPackSize)
	{
		nRelReadNum = recv(m_sockClient,pszbuf+ offset,nPackSize,0);
		nPackSize -= nRelReadNum;
		offset += nRelReadNum;
	}
	theApp.m_pKernel->DealData(pszbuf);
	delete []pszbuf;
	pszbuf= NULL;
}