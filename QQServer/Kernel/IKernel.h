#pragma once 

#include <WinSock2.h>
class IKernel
{
public:
	IKernel(void)
	{
	}

	~IKernel(void)
	{

	}
public:
	virtual bool Open() = 0;
	virtual void Close() = 0;
	virtual void DealData(char* szbuf,SOCKET sockWaiter) = 0;
};

