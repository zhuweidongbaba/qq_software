#pragma once
#include "INet.h"
#include "IKernel.h"
class TCPNet :public INet
{
public:
	TCPNet(IKernel *pKernel);
	~TCPNet(void);
public:
	bool InitNetWork();
	void UnInitNetWork();
	bool SendData(SOCKET sock,char *szbuf,int nLen);
	void RecvData(SOCKET sockWaiter);
public:
	static unsigned  __stdcall ThreadProc( void * );
	static unsigned  __stdcall ThreadRecv( void * );
private:
	SOCKET m_sockListen;
	HANDLE m_hThread;
	bool   m_bFlagQuit;
	map<unsigned,SOCKET> m_mapThreadIdToSocket;
	list<HANDLE> m_lstThread;
	IKernel *m_pKernel;
};

