// 0114QQClientDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "0114QQClient.h"
#include "0114QQClientDlg.h"
#include "afxdialogex.h"
#include "CdlgMain.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


CMy0114QQClientDlg::CMy0114QQClientDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CMy0114QQClientDlg::IDD, pParent)
	, m_edtusername(_T(""))
	, m_edtpassword(_T("123456789"))
	, m_edtqq(1)
	, m_edttel()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMy0114QQClientDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDT_NAME, m_edtusername);
	DDX_Text(pDX, IDC_EDT_PASSWORD, m_edtpassword);
	DDX_Text(pDX, IDC_EDT_QQ, m_edtqq);
	DDX_Text(pDX, IDC_EDT_TEL, m_edttel);
}

BEGIN_MESSAGE_MAP(CMy0114QQClientDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_MESSAGE(UM_LOGINMSG,&CMy0114QQClientDlg::LoginRs)
	ON_MESSAGE(UM_REGISTERMSG,&CMy0114QQClientDlg::RegisterRs)
	ON_BN_CLICKED(IDC_BUTTON1, &CMy0114QQClientDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CMy0114QQClientDlg::OnBnClickedButton2)
END_MESSAGE_MAP()

LRESULT CMy0114QQClientDlg::LoginRs(WPARAM W,LPARAM L)
{
	UpdateData(TRUE);
	CdlgMain dlg;
	theApp.m_pMainWnd = &dlg;
	dlg.m_qq = m_edtqq;
	//销毁登录页面
	EndDialog(IDOK);
	//弹出新窗口
	dlg.DoModal();
	return 0;
}
// CMy0114QQClientDlg 消息处理程序
LRESULT CMy0114QQClientDlg::RegisterRs(WPARAM W,LPARAM L)
{
	m_edtqq = *(long long *)W;
	UpdateData(FALSE);
	MessageBox("QQ号申请成功啦");
	return 0;
}

BOOL CMy0114QQClientDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CMy0114QQClientDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CMy0114QQClientDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文
		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);
		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CMy0114QQClientDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CMy0114QQClientDlg::OnBnClickedButton1()
{
	UpdateData(TRUE);
	STRU_REGISTER_RQ srr;
	srr.m_nType = _DEFFAULT_PROTOCOL_REGISTER_RQ;
	strcpy_s(srr.m_szName,sizeof(srr.m_szName),m_edtusername);
	strcpy_s(srr.m_szPassword,sizeof(srr.m_szPassword),m_edtpassword);
	srr.m_tel = m_edttel;
	theApp.m_pKernel->SendData((char*)&srr,sizeof(srr));
}

void CMy0114QQClientDlg::OnBnClickedButton2()
{
	UpdateData(TRUE);
	STRU_LOGIN_RQ slr;
	slr.m_nType=_DEFFAULT_PROTOCOL_LOGIN_RQ;
	slr.m_qq=m_edtqq;
	strcpy_s(slr.m_szPassword,sizeof(slr.m_szPassword),m_edtpassword);
	theApp.m_pKernel->SendData((char*)&slr,sizeof(slr));
}