#pragma once


#define _DEFFAULT_PROTOCOL_BASE    10

//申请qq号
#define _DEFFAULT_PROTOCOL_REGISTER_RQ    _DEFFAULT_PROTOCOL_BASE + 1
#define _DEFFAULT_PROTOCOL_REGISTER_RS    _DEFFAULT_PROTOCOL_BASE + 2
//登录
#define _DEFFAULT_PROTOCOL_LOGIN_RQ    _DEFFAULT_PROTOCOL_BASE + 3
#define _DEFFAULT_PROTOCOL_LOGIN_RS    _DEFFAULT_PROTOCOL_BASE + 4
//好友列表
#define _DEFFAULT_PROTOCOL_GETFRIENDLIST_RQ    _DEFFAULT_PROTOCOL_BASE + 5
#define _DEFFAULT_PROTOCOL_GETFRIENDLIST_RS    _DEFFAULT_PROTOCOL_BASE + 6
//聊天
#define _DEFFAULT_PROTOCOL_SELECTFRIENDCHAT_RQ    _DEFFAULT_PROTOCOL_BASE + 7
#define _DEFFAULT_PROTOCOL_SELECTFRIENDCHAT_RS    _DEFFAULT_PROTOCOL_BASE + 8

#define _login_noexist_user    0
#define _login_err_userpassword 1
#define _login_success          2
//协议包
#define MAXSIZE     45
#define FRIENDNUM    50
#define CONTENTSIZE   4096
#define SQLLEN  300
typedef char PackType;
//申请qq号
 struct STRU_REGISTER_RQ
{
	 PackType   m_nType;
	 long long  m_tel;
	 char      m_szName[MAXSIZE];
	 char      m_szPassword[MAXSIZE];
};

struct STRU_REGISTER_RS
{
	 PackType   m_nType;
	 long long  m_qq;
	 char       m_szResult;
};

//登录
 struct STRU_LOGIN_RQ
{
	 PackType   m_nType;
	 long long  m_qq;
	 char      m_szPassword[MAXSIZE];
};

struct STRU_LOGIN_RS
{
	 PackType   m_nType;
	 char      m_szResult;
};
//获取好友列表
 struct STRU_GETFRIENDLIST_RQ
{
	 PackType   m_nType;
	 long long  m_qq;
};

 struct FriendInfo
 {
	 long long  m_qq;
	 char      m_szName[MAXSIZE];
	 char      m_szState;
 };


 struct STRU_GETFRIENDLIST_RS
{
	 PackType   m_nType;
	 FriendInfo  m_aryFriendInfo[FRIENDNUM];
	 int        m_nFriendNum;
};

 //找朋友聊天
struct STRU_SELECTFRIENDCHAT_RQ
{
	 PackType   m_nType;
	 long long  m_qq;
	 long long  m_Friendqq;
	 char      m_szContent[CONTENTSIZE];
	 //int        m_nNum;  //校验成功还是失败
};
////回复
//struct STRU_SELECTFRIENDCHAT_RS
//{
//	 PackType   m_nType;
//	 long long  m_qq;
//	 long long  m_Friendqq;
//	 int        m_nNum;  //校验成功还是失败
//};





