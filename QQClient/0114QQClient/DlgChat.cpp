// DlgChat.cpp : 实现文件
//

#include "stdafx.h"
#include "0114QQClient.h"
#include "DlgChat.h"
#include "afxdialogex.h"
#include "CdlgMain.h"

// CDlgChat 对话框

IMPLEMENT_DYNAMIC(CDlgChat, CDialogEx)

CDlgChat::CDlgChat(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDlgChat::IDD, pParent)
	, m_edtsend(_T(""))
{

}

CDlgChat::~CDlgChat()
{
}

void CDlgChat::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_lstbox);
	DDX_Text(pDX, IDC_EDIT1, m_edtsend);
}


BEGIN_MESSAGE_MAP(CDlgChat, CDialogEx)
	ON_BN_CLICKED(IDC_BUTTON1, &CDlgChat::OnBnClickedButton1)
END_MESSAGE_MAP()


// CDlgChat 消息处理程序

void CDlgChat::OnBnClickedButton1()
{
	// TODO: 在此添加控件通知处理程序代码
	//获取朋友的qq
	CString strFriendQQ;
	GetWindowText(strFriendQQ);
	//获取聊天内容
	UpdateData(TRUE);
	STRU_SELECTFRIENDCHAT_RQ ssr;
	ssr.m_nType = _DEFFAULT_PROTOCOL_SELECTFRIENDCHAT_RQ;
	ssr.m_qq = ((CdlgMain*)theApp.m_pMainWnd)->m_qq;
	ssr.m_Friendqq = _atoi64(strFriendQQ);
	strcpy_s(ssr.m_szContent,sizeof(ssr.m_szContent),m_edtsend);
	if(theApp.m_pKernel->SendData((char*)&ssr,sizeof(ssr)))
	{
		m_lstbox.AddString("I say :");
		m_lstbox.AddString(m_edtsend);
		m_edtsend= _T("");
		UpdateData(FALSE);
	}
}
