#include "TCPNet.h"


TCPNet::TCPNet(IKernel *pKernel)
{
	m_sockListen = NULL;
	m_hThread = NULL; 
	m_bFlagQuit = true;
	m_pKernel=pKernel;
}


TCPNet::~TCPNet(void)
{
}


bool TCPNet::InitNetWork()
{
	//1.火锅  烤肉 串串  --加载库 
	WORD wVersionRequested;
    WSADATA wsaData;
    int err;

/* Use the MAKEWORD(lowbyte, highbyte) macro declared in Windef.h */
    wVersionRequested = MAKEWORD(2, 2);

    err = WSAStartup(wVersionRequested, &wsaData);
    if (err != 0) {
        /* Tell the user that we could not find a usable */
        /* Winsock DLL.                                  */   
        return false;
    }

/* Confirm that the WinSock DLL supports 2.2.*/
/* Note that if the DLL supports versions greater    */
/* than 2.2 in addition to 2.2, it will still return */
/* 2.2 in wVersion since that is the version we      */
/* requested.                                        */

    if (LOBYTE(wsaData.wVersion) != 2 || HIBYTE(wsaData.wVersion) != 2) {
        /* Tell the user that we could not find a usable */
        /* WinSock DLL.                                  */
     
        UnInitNetWork();
        return false;
    }
   
	//2.雇人（店长）--创建socket 
	m_sockListen =  socket(AF_INET ,SOCK_STREAM ,IPPROTO_TCP);
	if(INVALID_SOCKET == m_sockListen)
	{
		 UnInitNetWork();
        return false;
	}
	//3.找地  - - 
	sockaddr_in  addrServer;
	addrServer.sin_addr.S_un.S_addr = 0;
	addrServer.sin_family= AF_INET;
	addrServer.sin_port = htons(8899);
	if(SOCKET_ERROR == bind(m_sockListen,(const sockaddr*)&addrServer,sizeof(addrServer)))
	{
	    UnInitNetWork();
        return false;
	}
	//4.宣传  ---
	//获得CPU的核数
	SYSTEM_INFO si;
	GetSystemInfo(&si);
	if(SOCKET_ERROR == listen(m_sockListen,si.dwNumberOfProcessors*2))
	 {
		 UnInitNetWork();
         return false;
	 }

	//创建线程accept
	m_hThread =(HANDLE) _beginthreadex(0,0,&ThreadProc,this,0,0);
    if(m_hThread)
		 m_lstThread.push_back(m_hThread);

	return true;
}

unsigned  __stdcall TCPNet::ThreadProc( void * lpvoid)
{
	TCPNet *pthis = (TCPNet*)lpvoid;
	 unsigned  uthreadid;
	while(pthis->m_bFlagQuit)
	{
		SOCKET sockWaiter =  accept(pthis->m_sockListen,0,0);
		
		//为每个人分配一个线程
		 HANDLE hThread =(HANDLE) _beginthreadex(0,0,&ThreadRecv,pthis,0,&uthreadid);
		 if(hThread)
		 {
			 pthis->m_lstThread.push_back(hThread);
			 pthis->m_mapThreadIdToSocket[uthreadid] = sockWaiter;
		 }
	}
	return 0;
}

unsigned  __stdcall TCPNet::ThreadRecv( void * lpvoid)
{
	TCPNet *pthis = (TCPNet*)lpvoid;
	SOCKET sockWaiter =  pthis->m_mapThreadIdToSocket[GetCurrentThreadId()];
	pthis->RecvData(sockWaiter);
	
	return 0;
}

void TCPNet::UnInitNetWork()
{
	
	m_bFlagQuit = false;
	auto ite = m_lstThread.begin();
	while(ite != m_lstThread.end())
	{
		if(*ite)
		{
			if(WAIT_TIMEOUT == WaitForSingleObject(*ite,100))
				TerminateThread(*ite,-1);

			CloseHandle(*ite);
			*ite = NULL;
		}
		ite++;
	}
	m_lstThread.clear();
	
	if(m_sockListen)
	{
		closesocket(m_sockListen);
		m_sockListen = NULL;
	}
	 WSACleanup();
}

bool TCPNet::SendData(SOCKET sock,char *szbuf,int nLen)
{
	if(!sock || !szbuf ||nLen <=0 )
		return false;
	//发送包的大小
	if(send(sock,(const char*)&nLen,sizeof(int),0) <=0)
		return false;

	//发送数据
	if(send(sock,szbuf,nLen,0) <=0)
		return false;
	return true;
}

void TCPNet::RecvData(SOCKET sockWaiter)
{
	int nPackSize;
	int nRelReadNum;
	
	char* pszbuf= NULL;
	while(m_bFlagQuit)
	{
		//接收数据--大小
		nRelReadNum = recv(sockWaiter,(char*)&nPackSize,sizeof(int),0);
		if(nRelReadNum <=0)
		{
			//如果客户端下线了
			if(GetLastError() == WSAECONNRESET)
			{
				//删除映射
			   auto ite =	m_mapThreadIdToSocket.begin();
				while(ite !=	m_mapThreadIdToSocket.end())
				{
					if(ite->second == sockWaiter)
					{
						m_mapThreadIdToSocket.erase(ite);
						//关闭waiter 
						closesocket(sockWaiter);
						sockWaiter = NULL;
						return;//退出线程
					}
					ite++;
				}			
			}
			continue;
		}
		
		//接收数据包
		pszbuf = new char[nPackSize];
		int offset = 0;
		while(nPackSize)
		{
			nRelReadNum = recv(sockWaiter,pszbuf+ offset,nPackSize,0);
			nPackSize -= nRelReadNum;
			offset += nRelReadNum;
		}

		//处理
		m_pKernel->DealData(pszbuf,sockWaiter);
		delete []pszbuf;
		pszbuf = NULL;

	}
}